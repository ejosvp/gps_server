<?php

require_once __DIR__."/app/bootstrap.php";

use GPServer\GPServer;

$server = new GPServer(true);
$server->run();
