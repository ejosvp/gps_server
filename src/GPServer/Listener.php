<?php

namespace GPServer;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GPServer\Entity\Data;

class Listener implements MessageComponentInterface {

    private $container;
    private $timer;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->counter = 0;
    }

    public function onOpen(ConnectionInterface $conn) {
        //Se registra la conección
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        /** @var $em \Doctrine\ORM\EntityManager */
        $em = $this->container->get('entity_manager');
        $matcher = $this->container->get('matcher');
        /** @var $driver DriverInterface */
        $hexmsg = bin2hex($msg);
        if ($driver = $matcher->evaluate($hexmsg)) {
            $data = new Data();
            $driver->parse($hexmsg, $data);
            $em->persist($data);
            $this->counter++;
            $now = new \DateTime('now');
            if ($this->counter % $this->container->getParameter('server.flush') == 0) {
                $em->flush();
                echo "FLUSH!! [". $this->counter ."]\n";
            } else {
                echo ".";
            }
        } else {
            $this->container->get('logger')->addWarning(sprintf('Message not handle: %s', $msg));
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // Se registra el cierre de la conección
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}
