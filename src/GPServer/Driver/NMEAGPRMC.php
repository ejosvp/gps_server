<?php

namespace GPServer\Driver;

use GPServer\Entity\Data;

abstract class NMEAGPRMC extends AbstractDriver
{
    public function getPattern()
    {
        return '/^\$GPRMC,(\d{6}\.\d{2}),([AV]{1}),(\d{4}\.\d{2}),[NS]{1},(\d{4}\.\d{2}),[EW]{1},(\d{3}\.\d{1}),(\d{3}\.\d{1}),(\d{6}),(\d{3}\.\d{1}),([EW]{1})\*[A-F0-9]{2}|(.*)$/';
    }
}
