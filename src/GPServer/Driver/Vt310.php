<?php

namespace GPServer\Driver;

use GPServer\Entity\Data;

class Vt310 extends NMEAGPRMC
{

    public function getPattern()
    {
        return '/^2424007.(?<id>[\dA-Fa-f]{1,14})9955(?<time>[\dA-Fa-f]{12}2e[\dA-Fa-f]{6})2c(?<status>41|56)2c(?<latitude>[\dA-Fa-f]{8}2e[\dA-Fa-f]{8})2c(?<laorientation>53|4E)2c(?<longitude>[\dA-Fa-f]{10}2e[\dA-Fa-f]{8})2c(?<loorientation>57|45)2c(?<speed>[\dA-Fa-f]{2,4}2e[\dA-Fa-f]{4})2c(?<heading>[\dA-Fa-f]{2,6}(2e[\dA-Fa-f]{0,4})?)2c(?<date>[\dA-Fa-f]{12})2c(?<magvariation>[\dA-Fa-f]{2,6}2e[\dA-Fa-f]{2,4})?2c(?<magorientation>57|45)?2a(?<checksum>[\dA-Fa-f]{4})7c(?<sensors>.*)/';
    }

    public function getName()
    {
        return 'Meitrack VT310';
    }

    public function parse($message, Data $data)
    {
        $matches = $this->match($message);
        $data->setTrackId( hex2bin($matches['id']));
        $data->setTimestamp(\DateTime::createFromFormat('His.u', hex2bin($matches['time'])));
        $data->setDataValidity( hex2bin($matches['status']));
        $data->setLatitude( (hex2bin($matches['laorientation']) == 'S') ? ((float) '-'.hex2bin($matches['latitude'])/100) : ((float) $matches['latitude'])/100);
        $data->setLongitude( (hex2bin($matches['loorientation']) == 'W') ? ((float) '-'.hex2bin($matches['longitude'])/100) : ((float) $matches['longitude'])/100);
        $data->setSpeedOverGround( (float) hex2bin($matches['speed']));
        $data->setTrueCourse( (float) hex2bin($matches['heading']));
        $data->setDatestamp(\DateTime::createFromFormat('dmy', hex2bin($matches['date'])));
        $data->setChecksum( hex2bin($matches['checksum']));

        $sensors = hex2bin($matches['sensors']);
        $sensors = explode('|', trim($sensors, '|'));
        $data->setSensors($sensors);

        $data->setDriver($this->getName());
        $data->setMessage($message);
    }
}
