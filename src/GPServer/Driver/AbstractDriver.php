<?php

namespace GPServer\Driver;

use GPServer\DriverInterface;

abstract class AbstractDriver implements DriverInterface
{
    public function match($message)
    {
        $pattern = $this->getPattern();
        preg_match($pattern, $message, $matches);
        return ($matches == array()) ? false : $matches;
    }
}
