<?php

namespace GPServer\Driver;

use GPServer\Entity\Data;

class Vt340 extends NMEAGPRMC
{
    public function parse($message, Data $data)
    {
        $matches = $this->match($message);
        $data->setTrackId( hex2bin($matches['id']));
        $data->setTimestamp(\DateTime::createFromFormat('His.u', (hex2bin($matches['time']) . '.000')));
        $data->setDataValidity( hex2bin($matches['status']));
        $data->setLatitude( (float) hex2bin($matches['latitude']));
        $data->setLongitude( (float) hex2bin($matches['longitude']));
        $data->setSpeedOverGround( (float) hex2bin($matches['speed']));
        $data->setTrueCourse( (float) hex2bin($matches['heading']));
        $data->setDatestamp(\DateTime::createFromFormat('dmy', hex2bin($matches['date'])));
        $data->setChecksum( hex2bin($matches['checksum']));

        $sensors = hex2bin($matches['sensors']);
        $sensors = explode('|', trim($sensors, '|'));
        $data->setSensors($sensors);

        $data->setDriver($this->getName());
        $data->setMessage($message);
    }

    public function getPattern()
    {
        return '/^2424(?<flag>[\dA-Fa-f]{2})(?<l>[\dA-Fa-f]{2,6})2c(?<id>[\dA-Fa-f]{30})2c4141412c(?<code>[\dA-Fa-f]{2,4})2c(?<latitude>(2d)?[\dA-Fa-f]{2,4}2e[\dA-Fa-f]{12})2c(?<longitude>(2d)?[\dA-Fa-f]{2,6}2e[\dA-Fa-f]{12})2c(?<date>[\dA-Fa-f]{12})(?<time>[\dA-Fa-f]{12})2c(?<status>41|56)2c(?<nsatellites>[\dA-Fa-f]{2,4})2c(?<gsignal>[\dA-Fa-f]{2,4})2c(?<speed>[\dA-Fa-f]{2,6})2c(?<heading>[\dA-Fa-f]{2,6})2c(?<hdop>[\dA-Fa-f]{2,4}2e[\dA-Fa-f]{2})2c(?<altitude>[\dA-Fa-f]{2,8})2c(?<mileage>[\dA-Fa-f]{2,20})2c(?<runtime>[\dA-Fa-f]{2,20})2c(?<sensors>.*)2c2a(?<checksum>[\dA-Fa-f]{4})0d0a/';
    }

    public function getName()
    {
        return 'Meitrack VT340';
    }

}
