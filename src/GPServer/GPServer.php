<?php

namespace GPServer;

require_once __DIR__.'/../../app/bootstrap.php';

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\Yaml\Parser as YamlParser;
use Symfony\Component\Yaml\Exception\ParseException as YamlParseException;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Configuration;
use Doctrine\Common\Cache\ArrayCache as Cache;
use Doctrine\Common\Annotations\AnnotationRegistry;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

use Ratchet\Server\IoServer;

class GPServer
{
    private $dev;
    /** @var $container ContainerBuilder */
    private $container;

    public function __construct($dev = false)
    {
        $this->dev = $dev;

        $this->loadContainer();

        $this->registerConfig();
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function configDoctrine($params)
    {
        $config = new Configuration();
        $cache = new Cache();
        $config->setQueryCacheImpl($cache);
        $config->setProxyDir(__DIR__.'/EntityProxy');
        $config->setProxyNamespace('EntityProxy');
        $config->setAutoGenerateProxyClasses(true);

        $paths = array(__DIR__.'/Entity');

        $config = Setup::createAnnotationMetadataConfiguration($paths, true);
        $em = EntityManager::create($params, $config);

        $this->container->set('entity_manager', $em);
    }

    public function loadContainer()
    {
        $this->container = new ContainerBuilder();
        $this->container->set('server', $this);
    }

    public function registerConfig()
    {
        $logger = new Logger('gps');
        $logger->pushHandler(new StreamHandler(__DIR__.sprintf('/../../app/logs/%s.log', $this->dev ? 'dev' : 'prod'),
            $this->dev ? Logger::ERROR : Logger::WARNING));
        $this->container->set('logger', $logger);

        $parser = new YamlParser();
        $this->container->set('yaml.parser', $parser);

        $config = array();

        try {
            $config = $parser->parse(file_get_contents(__DIR__.'/../../app/config.yml'));
        } catch (YamlParseException $e) {
            printf("Error al leer la configuración: %s", $e->getMessage());
        }

        $this->container->setParameter('server.flush', $config['server']['flush']);

        $this->container->set('listener', new Listener($this->container));
        $matcher = new Matcher($this->container);
        $this->container->set('matcher', $matcher);

        foreach ($config['drivers'] as $key => $class) {;
            $matcher->registerDriver(new $class);
        }

        $this->container->setParameter('server.port', $config['server']['port']);

        $this->configDoctrine($config['doctrine']);
    }

    public function startListener()
    {
        /** @var $server IoServer */
        $server = IoServer::factory(
            $this->container->get('listener'),
            $this->container->getParameter('server.port')
        );

        $server->run();
    }

    public function run()
    {
        $this->startListener();
    }

}
