<?php

namespace GPServer;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Matcher
{
    private $drivers;
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->drivers = new ArrayCollection();
        $this->container = $container;
    }

    public function registerDriver($driver)
    {
        $this->drivers->add($driver);
    }

    public function evaluate($msg)
    {
        foreach ($this->drivers as $driver) {
            /** @var $driver DriverInterface */
            if ($driver->match($msg))
                return $driver;
        }
        return false;
    }
}
