<?php

namespace GPServer;

use GPServer\Entity\Data;

interface DriverInterface
{
    public function parse($message, Data $data);

    public function match($message);

    public function getPattern();

    public function getName();
}
