<?php

namespace GPServer\Entity;

use Doctrine\ORM\Mapping\Entity,
    Doctrine\ORM\Mapping\Id,
    Doctrine\ORM\Mapping\Column,
    Doctrine\ORM\Mapping\GeneratedValue,
    Doctrine\ORM\Mapping\Table;

use Doctrine\ORM\Mapping;

/**
 * @Entity
 * @Table(name="gps_data")
 */
class Data
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Column(type="string", length=255)
     */
    private $trackId;

    /**
     * @Column(type="time")
     */
    private $timestamp;

    /**
     * @Column(type="boolean")
     */
    private $dataValidity;

    /**
     * @Column(type="decimal", scale=5)
     */
    private $latitude;

    /**
     * @Column(type="decimal", scale=5)
     */
    private $longitude;

    /**
     * @Column(type="decimal", scale=2)
     */
    private $speedOverGround;

    /**
     * @Column(type="decimal", scale=2)
     */
    private $trueCourse;

    /**
     * @Column(type="date")
     */
    private $datestamp;

    /**
     * @Column(type="decimal", scale=1, nullable=true)
     */
    private $magneticVariation;

    /**
     * @Column(type="string", length=1, nullable=true)
     */
    private $magneticVariationDirection;

    /**
     * @Column(type="string", length=2)
     */
    private $checksum;

    /**
     * @Column(type="array")
     */
    private $sensors;

    /**
     * @Column(type="string", length=255)
     */
    private $driver;

    /**
     * @Column(type="text")
     */
    private $message;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setTrackId($trackId)
    {
        $this->trackId = $trackId;
    }

    public function getTrackId()
    {
        return $this->trackId;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return Data
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set dataValidity
     *
     * @param boolean $dataValidity
     * @return Data
     */
    public function setDataValidity($dataValidity)
    {
        $this->dataValidity = $dataValidity;

        return $this;
    }

    /**
     * Get dataValidity
     *
     * @return boolean
     */
    public function getDataValidity()
    {
        return $this->dataValidity;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Data
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Data
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set speedOverGround
     *
     * @param float $speedOverGround
     * @return Data
     */
    public function setSpeedOverGround($speedOverGround)
    {
        $this->speedOverGround = $speedOverGround;

        return $this;
    }

    /**
     * Get speedOverGround
     *
     * @return float
     */
    public function getSpeedOverGround()
    {
        return $this->speedOverGround;
    }

    /**
     * Set trueCourse
     *
     * @param float $trueCourse
     * @return Data
     */
    public function setTrueCourse($trueCourse)
    {
        $this->trueCourse = $trueCourse;

        return $this;
    }

    /**
     * Get trueCourse
     *
     * @return float
     */
    public function getTrueCourse()
    {
        return $this->trueCourse;
    }

    /**
     * Set datestamp
     *
     * @param \DateTime $datestamp
     * @return Data
     */
    public function setDatestamp($datestamp)
    {
        $this->datestamp = $datestamp;

        return $this;
    }

    /**
     * Get datestamp
     *
     * @return \DateTime
     */
    public function getDatestamp()
    {
        return $this->datestamp;
    }

    /**
     * Set magneticVariation
     *
     * @param float $magneticVariation
     * @return Data
     */
    public function setMagneticVariation($magneticVariation)
    {
        $this->magneticVariation = $magneticVariation;

        return $this;
    }

    /**
     * Get magneticVariation
     *
     * @return float
     */
    public function getMagneticVariation()
    {
        return $this->magneticVariation;
    }

    /**
     * Set magneticVariationDirection
     *
     * @param boolean $magneticVariationDirection
     * @return Data
     */
    public function setMagneticVariationDirection($magneticVariationDirection)
    {
        $this->magneticVariationDirection = $magneticVariationDirection;

        return $this;
    }

    /**
     * Get magneticVariationDirection
     *
     * @return boolean
     */
    public function getMagneticVariationDirection()
    {
        return $this->magneticVariationDirection;
    }

    /**
     * Set checksum
     *
     * @param string $checksum
     * @return Data
     */
    public function setChecksum($checksum)
    {
        $this->checksum = $checksum;

        return $this;
    }

    /**
     * Get checksum
     *
     * @return string
     */
    public function getChecksum()
    {
        return $this->checksum;
    }

    public function setSensors($sensors)
    {
        $this->sensors = $sensors;
    }

    public function getSensors()
    {
        return $this->sensors;
    }

    public function setDriver($driver)
    {
        $this->driver = $driver;
    }

    public function getDriver()
    {
        return $this->driver;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }

}
