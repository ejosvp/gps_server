# Makefile
#
#

zombie_test:
	python util/zombieab/manager.py 127.0.0.1 22222 10 10 1000000 5

run:
	php gps_dev.php

database:
	php bin/doctrine.php orm:schema-tool:drop
	php bin/doctrine.php orm:schema-tool:update --force

clearlogs:
	rm app/logs/*
