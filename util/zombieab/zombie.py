import decimal
import random
import socket
import time
import threading
import datetime
import sys
from operator import xor


class Zombie:
    sleepTime = 5
    percentageOutside = 10
    velocity = 100
    lastLatitude = 0
    lastLongitude = 0
    port = 1337
    ip = "127.0.0.1"
    nsend = 1
    imei = 0000000000000000

    queue = []

    def __init__(self, host, port, nsend, imei, sleepTime):
        self.ip = host
        self.port = port
        self.nsend = nsend
        self.imei = imei
        self.sleepTime = sleepTime

    def changePosition(self):
        self.lastLatitude += decimal.Decimal(random.randint(0, self.velocity)) / 10000
        self.lastLongitude += decimal.Decimal(random.randint(0, self.velocity)) / 10000

    def defineInitialPosition(self):
        latitude = random.randint(1880, 5900)
        longitude = random.randint(10100, 17900)
        self.lastLatitude = decimal.Decimal('%d.%d' % (latitude, random.randint(1000, 9999)))
        self.lastLongitude = decimal.Decimal('%d.%d' % (longitude, random.randint(1000, 9999)))

    def generatePlot(self):
        loc = 'N' if self.lastLatitude < 0 else 'S'
        lac = 'E' if self.lastLongitude < 0 else 'W'

        if self.lastLatitude < 0:
            self.lastLatitude = self.lastLatitude * -1

        if self.lastLongitude < 0:
            self.lastLongitude = self.lastLongitude * -1

        hoy = datetime.datetime.now()
        code = hoy.strftime("%H%M%S")
        f = hoy.strftime("%d%m%y")
        intro = "%c%c%c%c" % (0x24, 0x24, 0x00, 0x7e)
        trace = "%s%cU%s.000,A,%s,%s,%s,%s,58.31,309.62,%s,,*" % (self.imei, 0x99, code, self.lastLatitude, loc, self.lastLongitude, lac, f)
        nmea = map(ord, trace[1:trace.index('*')])
        checksum = reduce(xor, nmea)
        cs = hex(checksum).rstrip("L").lstrip("0x")
        plus = "|1.0|2462|0000|0006,0006|02CC0006151AC5C8|0E|00001BEE"
        return "%s%s%s%s\n" % (intro, trace, cs, plus)

    def sendData(self):
        for i in range(self.nsend):
            self.changePosition()
            self.queue.append(self.generatePlot())
            if(random.randint(0, 100) > self.percentageOutside):
                s = socket.socket()
                try:
                    s.connect((self.ip, self.port))
                    for plot in self.queue:
                        print plot
                        s.send(plot)
                    self.queue = []
                    s.close()
                except Exception, e:
                    print 'something\'s wrong with %s:%d. Exception type is %s' % (self.ip, self.port, `e`)
                    sys.exit(1)
            time.sleep(self.sleepTime)
