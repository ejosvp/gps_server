import threading
import sys
from zombie import Zombie

sname, host, port, ngps, nstreams, startid, sleep = sys.argv

class ZombieThread(threading.Thread):
    def run(self):
        zmb = Zombie(host, int(port), int(nstreams), id, sleepTime)
        zmb.defineInitialPosition()
        zmb.sendData()

if __name__ == "__main__":
    id = int(startid)
    sleepTime = int(sleep)
    for i in range(int(ngps)):
        t = ZombieThread()
        t.start()
        id += 1
